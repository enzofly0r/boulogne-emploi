<div class="container">
    <div class="row">
        <div class="col s12 m12">
            <a href="gest-actus.php">
                <div class="card add-ressources">
                    <i class="medium material-icons">library_add</i>
                </div>
            </a>
        </div>
        <div class="col s12" id="actus-content">
            <?php
            $reponse = $bdd->query('SELECT * FROM be_actualites ORDER BY id_actus DESC');
            while ($donnees = $reponse->fetch()) {
                echo '
        <div class="col s12 card grey lighten-3">
        <div class="card-content">
        <!--*****DIV IMAGE*****-->
        <div class="col s12 m2" id="imgactu">
        <img src="traitement/' . $donnees['picture_url'] . '">
        </div>
        <!--*****DIV TITRE*****-->
        <div class="col s12 m2">
        <span class="card-title">Titre</span><br>
        <p>' . $donnees['title_actu'] . '</p>
        </div>
        <!--*****DIV DETAILS*****-->
        <div class="col s12 m3">
        <span class="card-title">Détails</span><br>
        <p class="details">' . $donnees['description'] . '</p>
        </div>
        <!--*****DIV AUTEUR*****-->
        <div class="col s6 m2">
        <span class="card-title">Auteur</span><br>
        <p>' . $donnees['users_id'] . '</p>
        </div>
        <!--*****DIV DATE*****-->
        <div class="col s6 m2">
        <span class="card-title">Date</span><br>
        <p>' . $donnees['date'] . '</p>
        </div>
        <!--*****DIV MODIF*****-->
        <div class="col s2 m1">
        <a href="update-actus.php?idactu=' . $donnees['id_actus'] . '"><i class="small material-icons">edit</i></a>
        <br>
        </div>
        <div class="col s2 m1">
        <a href="#"><i id="' . $donnees['id_actus'] . '" class="delete-actu small material-icons">delete</i></a>
        </div>

        </div>
        </div>
        ';
            }
            $reponse->closeCursor(); // Termine le traitement de la requête
            ?>
        </div>
    </div>
</div>
