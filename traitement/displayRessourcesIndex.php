<?php
if (isset($_POST['displayRessources'])) {
    /*
     * Redeclaration de l'objet datafront obligatoire lors d'un appel Ajax côté serveur
     */
    require 'class/Data.class.php';
    $pdo = new Data();
    $check = $_POST['displayRessources'];
    $donnees = $pdo->afficheRessourcesIndexById($check);
} else {
    $donnees = $pdo->afficheRessourcesIndex();
}
?>

<div class="ressources" style="padding: 0;">
    <?php
    foreach ($donnees as $response) {
        echo '
        <figure class="col s6 m6 imghvr-slide-left img-ressources transparent">
            <img src="administration/traitement/' . $response['picture_ressource'] . ' ">
                <figcaption style="background-color: rgba(0,0,0, 0.7)">
                <span class="col s12 center-align">' . $response['title_ressource'] . '</span>
                    ' . $response['description'] . '
                </figcaption>
            <a href="ressources.php?ressource='.$response['id_ressources'].'"></a>
        </figure>
        ';
    }
    ?>
</div>
