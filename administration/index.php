<?php
session_start();
if (!empty($_SESSION['email'])) {
    header('Location: home.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom-adm.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/administration.js" type="text/javascript"></script>
    </head>

    <body id="body">

        <?php
        if (!empty($_SESSION['errorCon'])) {
            $errorCon = $_SESSION['errorCon'];
            ?>
            <div class="row red white-text flow-text center-align" id="errorMsgAdmin"><?php echo $errorCon; ?></div>
            <?php
        }
        session_destroy();
        ?>

        <div class="content-index">
            <div class="row">
                <div class="col s12 m8 offset-m2 l6 offset-l3">
                    <figure class="profil-picture"><img src="https://s-media-cache-ak0.pinimg.com/736x/00/fd/7d/00fd7db068658d02d0f3576150f3bfbf.jpg"/></figure>
                </div>
            </div>
            <div class="row">
                <form method="POST" action="traitement/connexion.php" class="col s12 m8 offset-m2 l6 offset-l3">
                    <div class="row white-text">
                        <div class="input-field col s12">
                            <input name="email" id="email" type="email" class="validate">
                            <label for="email">Adresse e-mail</label>
                        </div>
                        <div class="input-field col s12">
                            <input name="password" id="password" type="password">
                            <label for="password">Mot de passe</label>
                        </div>
                    </div>
                    <div class="col s12 m12 l12 center-align">
                        <button type="submit" class="btn waves-effect waves-teal blue-grey darken-3"><i class="material-icons right">fast_forward</i>Se connecter</button>
                    </div>
                </form>
            </div>

        </div>

    </body>

</html>