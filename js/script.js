$(document).ready(function () {

    $(".button-collapse").sideNav();

    /*
     * Déclenche l'affinage des ressources sur l'index
     */
    $('body').on("click", ".displayRessourcesIndex", function () {
        var displayRessources = $(this).attr('id');
        $.ajax({
            url: 'traitement/displayRessourcesIndex.php',
            type: 'POST',
            data: {displayRessources: displayRessources},
            dataType: 'html',
            success: function ($response) {
                $(".ressources").empty();
                $(".ressources").hide().append($response).fadeIn().show();
            }
        });
    });

    /*
     * Déclenche l'affinage des ressources dans ressources
     */
    $('body').on("click", ".displayRessources", function () {
        var displayRessources = $(this).attr('id');
        $.ajax({
            url: 'traitement/displayRessources.php',
            type: 'POST',
            data: {displayRessources: displayRessources},
            dataType: 'html',
            success: function ($response) {
                $(".ressources").empty();
                $(".ressources").hide().append($response).fadeIn().show();
            }
        });
    });


    /*
     * Like actus
     */
    $('body').on("click", ".ratingHeart", function () {
       var ratingactus = $(this).attr('id');
       $.ajax({
           url: 'traitement/ratingactus.php',
           type: 'POST',
           data: {ratingactus: ratingactus},
           dataType: 'html',
           success: function () {
               $(".ratingHeart").addClass('red-text');
               $(".ratingHeart").effect("bounce", {times: 3}, 300);
               setTimeout(refreshHeart, 1000);
               function refreshHeart() {
               $(".refreshHeart").load(location.href+" .refreshHeart2");
             }
           }
       });
   });

});
