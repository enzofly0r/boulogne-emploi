<footer class="page-footer" style="background-color: #129D81;">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Lorem Ipsum</h5>
                <p class="grey-text text-lighten-4">
                    Dispendium levibus aegrum eius insontium fecit quassari existimans offensis quassari aegrum ita factum salutis aut etiam caedibus animus salutis insontium ad levibus aegrum suae dispendium tener aut corpus eius solet.
                </p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Plan du site</h5>
                <ul>
                    <li><a class="grey-text text-lighten-3" href="#!">Qui sommes-nous</a></li>
                    <li><a class="grey-text text-lighten-3" href="actualites.php">Actualités</a></li>
                    <li><a class="grey-text text-lighten-3" href="ressources.php">Ressources</a></li>
                    <li><a class="grey-text text-lighten-3" href="contact.php">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © 2016 Simplon.co
            <a class="grey-text text-lighten-4 right" href="administration/">Espace administration</a>
        </div>
    </div>
</footer>
