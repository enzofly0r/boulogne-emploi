<?php
session_start();
if (empty($_SESSION)) {
    die('404');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom-adm.css">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"  integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/administration.js" type="text/javascript"></script>
    </head>

    <body>

        <?php include("header.php"); ?>

        <?php include("edit-profil.php"); 
        
            
        ?>
        <form action="traitement/add-ressources.php" method="POST" enctype="multipart/form-data">
        <div class="row" id="form-add-ressources">
            <div class="col s12 green-custom flow-text white-text center-align" style="padding: 1vh;">Ajouter une ressource</div>
            <div class="col s12 m10  offset-m1">
                <div class="input-field col s12 m6">
                    <input id="title_ressources" name="title_ressources" type="text" class="validate">
                    <label for="title_ressources">Titre de la ressource</label>
                </div>
                <div class="input-field col s12 m6">
                    <input id="description_ressources" name="description_ressources" type="text" class="validate">
                    <label for="description_ressources">Description</label>
                </div>
                <div class="input-field col s12 m6">
                    <select id="cat_ressources" name="cat_ressources">
                        <?php
                        $result = $pdo->afficheRessource();
                        
                        foreach ($result as $results) {
                            echo '<option value="'. $results['id_cat_ressources'] .'">' . $results['title_cat_ressources'] . '</option>';
                        }
                        ?>
                    </select>
                    <label>Selectionner une catégorie</label>
                </div>
                <div class="col s12">
                    <textarea id="content_ressources" name="content_ressources"></textarea>
                </div>
                <input type="file" name="picture">
                <div class="col s12">
                    <button type="submit" class="btn waves-effect waves-light blue-grey darken-1 right" id="btn-add-ressources">Ajouter
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </div>
        </form>
    </body>

</html>
