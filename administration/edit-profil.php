<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>Editer votre profil</h4>
        <div class="row">
            <div class="col s12 m3 l3">
                <div class="card">
                    <div class="card-image">
                        <img src="<?php echo $_SESSION['picture_url']; ?>"/>
                        <span class="card-title"><?php echo $_SESSION['lastname'] . ' ' . $_SESSION['firstname']; ?></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m9 l9 flow-text">   
                    <i class=" small material-icons">person_pin</i> <?php echo $_SESSION['lastname'] . ' ' . $_SESSION['firstname']; ?>
                </div>
                <div class="col s12 m9 l9 flow-text">
                    <i class=" small material-icons">email</i> <?php echo $_SESSION['email']; ?>
                </div>
                <div class="col s12 m9 l9 flow-text">
                    <i class=" small material-icons">business</i> <?php echo $_SESSION['workplace']; ?>
                </div>
                <div class="col s12 m9 l9 flow-text">
                    <i class=" small material-icons">work</i> <?php echo $_SESSION['status']; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
    </div>
</div>