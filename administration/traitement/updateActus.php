<?php

session_start();
require 'class/data.class.php';
$pdo = new data();


/*
* fin de l'envoie
*/
if (!empty($_POST) AND !empty($_FILES)) {

  $actuCat = htmlspecialchars($_POST['cat_actus']);
  $actuTitre = htmlspecialchars($_POST['title_actus']);
  $actuDescription = htmlspecialchars($_POST['description_actus']);
  $actuContenu = ($_POST['content_actu']);
  $important = $_POST['importante'];
  $picture = $_FILES['picture'];

  $picture_name = $_FILES['picture']['name'];
  $picture_type = $_FILES['picture']['type'];
  $picture_size = $_FILES['picture']['size'];
  $picture_extension = strrchr($picture_name, ".");
  $good_extension = array('.jpg', '.jpeg', '.png', '.gif', '.JPG', '.JPEG', '.PNG', '.GIF');
  $picture_tmp_name = $_FILES['picture']['tmp_name'];
  $picture_url = 'galleryActu/'.$picture_name;

  $users_id = $_SESSION['lastname'].' '.$_SESSION['name'];

  if (!empty($actuCat) AND ! empty($actuTitre) AND ! empty($actuDescription) AND ! empty($actuContenu)) {

    
    if (!empty($picture_name)) {



      if ($picture_size < 5000000) {

        if (in_array($picture_extension, $good_extension)) {



          move_uploaded_file($picture_tmp_name, $picture_url);

          $pdo->updateActus($_GET['idactu'], $actuCat, $actuTitre, $actuDescription, $actuContenu, $picture_url, $users_id);
          header('Location: ../home.php#actus');

        }else {
          echo 'Extension non valide';
        }
      }else {
        echo 'Le fichier est trop lourd';
      }
    }

$pdo->updateActusNoImg($_GET['idactu'], $actuCat, $actuTitre, $actuDescription, $actuContenu, $users_id);
header('Location: ../home.php#actus');





    }else {
      echo 'Veuillez remplir tous les champs !';
    }
  }
