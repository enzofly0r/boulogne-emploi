<!DOCTYPE html>
<html>
    <head>
        <title>Boulogne Emploi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/imagehover.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/script.js" type="text/javascript"></script>
    </head>

    <body>

        <!--- header --->
        <?php include ("header.php"); ?>
        <!--- last actus --->
        <?php include ("actus-header.php"); ?>
        <!--- last actus --->
        <!--- header --->

        <!---   --->
        <div class="row" id="index-site">
            <div class="container">
                <!--- ressources --->
                <div class="col s12 m8">
                    <div class="col s12 white" id="ressources">
                        <!-- ressources refine bar -->
                        <div class="col s12 m3" id="refine-bar">
                            <ul>
                                <?php
                                $donnees = $pdo->afficheAllCategorieRessources();
                                foreach ($donnees as $reponse) {
                                    echo '
                <li class="col s6 m12">
                <input name="group1" class="displayRessourcesIndex" type="radio" id="' . $reponse['id_cat_ressources'] . '" />
                <label for="' . $reponse['id_cat_ressources'] . '">' . $reponse['title_cat_ressources'] . '</label>
                </li>
                ';
                                }
                                ?>
                                <!-- btn refine -->
                            </ul>
                        </div>
                        <!-- ressources refine bar -->
                        <div class="col s12 m9" id="ressources-choice" style="padding-right: 0px;">
                            <?php include("traitement/displayRessourcesIndex.php"); ?>
                            <!-- ressources choices -->
                            <!-- ressources choices -->
                        </div>
                    </div>
                    <!--- qui sommes-nous -->
                    <div class="col s12 white" id="about">
                        <!-- title div box -->
                        <span class="col s12 grey-text text-darken-1 title center-align">QUI SOMMES-NOUS</span>
                        <p>Beneficio laus quidem suspicionibus auctoritatem quidem in iudicio concessisti ipso hodierno accepto suspicionibus commemoratis omnis in quo consensu iudicio maximum iudicio quanta tuis vitae paulo in tanta laus vel tum.</p>
                        <p>Beneficio laus quidem suspicionibus auctoritatem quidem in iudicio concessisti ipso hodierno accepto suspicionibus commemoratis omnis in quo consensu iudicio maximum iudicio quanta tuis vitae paulo in tanta laus vel tum.</p>
                        <p>Beneficio laus quidem suspicionibus auctoritatem quidem in iudicio concessisti ipso hodierno accepto suspicionibus commemoratis omnis in quo consensu iudicio maximum iudicio quanta tuis vitae paulo in tanta laus vel tum.</p>
                        <div class="col s12 right-align"><a href="about.php">Lire +</a></div>
                    </div>
                    <!--- qui sommes-nous -->
                    <div class="col s12 m6" style="padding-left: 0;">
                        <!--- bus de lemploi -->
                        <div class="col s12 white" id="busEmploi">
                            <!-- title div box -->
                            <span class="col s12 grey-text text-darken-1 title center-align">LE BUS DE L'EMPLOI</span>
                            <p>Beneficio laus quidem suspicionibus auctoritatem quidem in iudicio concessisti ipso hodierno accepto suspicionibus commemoratis omnis in quo consensu iudicio maximum iudicio quanta tuis vitae paulo in tanta laus vel tum.</p>
                            <div class="col s12 right-align"><a href="about.php">Lire +</a></div>
                        </div>
                    </div>
                    <!--- bus de lemploi -->
                    <!--- bus de lemploi -->
                    <div class="col s12 m6" style="padding-right: 0;">
                        <div class="col s12 white" id="maptoomi">
                            <!-- title div box -->
                            <span class="col s12 grey-text text-darken-1 title center-align">MAPTOOMI</span>
                            <p>Beneficio laus quidem suspicionibus auctoritatem quidem in iudicio concessisti ipso hodierno accepto suspicionibus commemoratis omnis in quo consensu iudicio maximum iudicio quanta tuis vitae paulo in tanta laus vel tum.</p>
                            <div class="col s12 right-align"><a href="about.php">Lire +</a></div>
                        </div>
                        <!--- bus de lemploi -->

                    </div>
                    <!-- partenaires -->
                    <div class="col s12 white" id="about">
                        <!-- title div box -->
                        <span class="col s12 grey-text text-darken-1 title center-align">PARTENAIRES</span>
                        <div class="col s4"><img src="webgallery/financeurs/cg62.png" style="width: 100%;"/></div>
                        <div class="col s4"><img src="webgallery/financeurs/reussir.png" style="width: 100%;"/></div>
                        <div class="col s4"><img src="webgallery/financeurs/eu.png" style="width: 100%;"/></div>
                        <div class="col s4"><img src="webgallery/financeurs/cget.png" style="width: 100%;"/></div>
                        <div class="col s4"><img src="webgallery/financeurs/NPDCalaisFSE.png" style="width: 100%;"/></div>
                        <div class="col s4"><img src="webgallery/financeurs/conseilRegio.png" style="width: 100%;"/></div>
                    </div>
                    <!-- partenaires -->

                </div>
                <!--- ressources --->

                <!--- recent post --->
                <div class="col s12 m4">
                    <div class="col s12 white" id="recent-post">
                        <!-- title div box -->
                        <span class="col s12 grey-text text-darken-1 title center-align">DERNIÈRE ACTUALITÉS</span>
                        <!-- post -->
                        <?php include "navigation_actualites.php" ?>
                        <!-- post -->
                        <div id="facebook" class="center-align">
                            <span class="col s12 grey-text text-darken-1 title center-align">FACEBOOK <i class="fa fa-facebook-official blue-text text-darken-1" aria-hidden="true"></i></span>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id))
                                        return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-page" data-href="https://www.facebook.com/boulogneemploi/" data-tabs="timeline, message" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/boulogneemploi/"><a href="https://www.facebook.com/boulogneemploi/">Boulogne emploi</a></blockquote></div></div>
                        </div>
                        <div id="twitter">
                            <span class="col s12 grey-text text-darken-1 title center-align">TWITTER <i class="fa fa-twitter blue-text text-lighten-2" aria-hidden="true"></i></span>
                            <a class="twitter-timeline"  href="https://twitter.com/REemploi1" data-widget-id="734722062309285889">Tweets de @REemploi1</a>
                            <script>!function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + "://platform.twitter.com/widgets.js";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, "script", "twitter-wjs");
                            </script>
                        </div>
                    </div>
                </div>
                <!--- recent post --->

            </div>
        </div>
        <?php include "footer.php"; ?>
    </body>

</html>
