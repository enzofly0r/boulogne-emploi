<?php
require 'traitement/class/Data.class.php';
$pdo = new Data();
?>
<div class="row" id="header">
    <div class="col s12 colortwo white-text" id="header-top">
        <div class="container">
            <div class="s6 m3 right">
                <ul>
                    <a href="https://www.facebook.com/boulogneemploi" target="_blank"><li class="fa fa-facebook" aria-hidden="true"></li></a>
                    <a href="#"><li class="fa fa-twitter" aria-hidden="true"></li></a>
                    <a href="contact.php"><li class="fa fa-map-marker" aria-hidden="true"></li></a>
                    <a href="contact.php"><li class="fa fa-envelope-o" aria-hidden="true"></li></a>
                </ul>
            </div>
        </div>
    </div>
    <div class="col s12 colorone white-text" id="nav">
        <div class="container">
            <div class="col s10 m4 flow-text">BOULOGNE EMPLOI</div>
            <ul class="right hide-on-med-and-down">
                <li><a href="index.php">ACCUEIL</a></li>
                <li><a href="actualites.php">ACTUALITÉS</a></li>
                <li><a href="ressources.php">RESSOURCES</a></li>
                <li><a href="about.php">QUI SOMMES-NOUS</a></li>
                <li><a href="contact.php">CONTACT</a></li>
            </ul>
            <ul id="slide-out" class="side-nav">
                <li><a href="index.php">ACCUEIL</a></li>
                <li><a href="actualites.php">ACTUALITÉS</a></li>
                <li><a href="ressources.php">RESSOURCES</a></li>
                <li><a href="about.php">QUI SOMMES-NOUS</a></li>
                <li><a href="contact.php">CONTACT</a></li>
            </ul>
            <a href="#" data-activates="slide-out" id="open-menu-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>

        </div>
    </div>
</div>
