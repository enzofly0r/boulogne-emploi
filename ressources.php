<!DOCTYPE html>
<html>
    <head>
        <title>Boulogne Emploi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/imagehover.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/script.js" type="text/javascript"></script>
    </head>

    <body>

        <!--- header --->
        <?php include ("header.php"); ?>
        <header id="header-ressources"></header>
        <!--- header --->

        <!---   --->
        <div class="row" id="ressources-site">
            <?php
            if (!isset($_GET['ressource'])) { // Affiche la barre d'affinage si GET N'est pas définie
                echo '
      <div class="col s12 white center-align" id="refine-bar">
      <ul>
      ';
                $donnees = $pdo->afficheAllCategorieRessources();
                foreach ($donnees as $reponse) {
                    echo '
        <li>
        <input name="group1" class="displayRessources" type="radio" id="' . $reponse['id_cat_ressources'] . '" />
        <label for="' . $reponse['id_cat_ressources'] . '">' . $reponse['title_cat_ressources'] . '</label>
        <i class="fa ' . $reponse['icon_url'] . '" aria-hidden="true"></i>
        </li>
        ';
                }
                echo '
      </ul>
      </div>
      ';
            } else { // Cache la barre d'affinage et propose un retour au site
                echo '
      <div class="col s12 white" style="padding: 10px;">
      <div class="container">
      <a href="ressources.php" class="btn-flat">Retour <i class="fa fa-reply" aria-hidden="true" style="font-size: 15px;"></i></a>
      </div>
      </div>
      ';
            }
            ?>
            <!-- btn refine -->
            <div class="container">
                <!-- actualites all -->
                <?php
                if (!isset($_GET['ressource'])) { // Affiche toutes les ressources si GET N'est pas définie
                    echo '
        <div class="col s12 m12">
        <div class="col s12 m12 white" id="ressources">';
                    include 'traitement/displayRessources.php';
                    echo '
        </div>
        </div>';
                } else { // Affiche une ressource définie par l'id dans le GET
                    ?>
                    <?php
                    $donnees = $pdo->afficheRessourcesById();
                    foreach ($donnees as $reponse) {
                        $title = $reponse['title_ressource'];
                        echo '
          <div class="col s12 m12">
          <div class="col s12 white ressources">
          <span class = "col s12 grey-text text-darken-1 title center-align">' . strtoupper($title) . '</span>
          <div class = "col s12" id = "ressources-details">
          <img style="height: 220px;" src = "administration/traitement/' . $reponse['picture_ressource'] . ' "/>
          <div class = "col s12 grey-text text-darken-1 no-padding"> ' . $reponse['description'] . '<br></div>
          ' . $reponse['content_ressource'] . '
          <div class = "col s6 grey-text text-darken-1 no-padding">' . $reponse['date'] . '</div>
          </div>
          </div>
          </div>
          ';
                    }
                    ?>

                    <?php
                }
                ?>
                <!--- actu détails -->
            </div>
        </div>
        <?php include "footer.php"; ?>
    </body>

</html>
