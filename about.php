<!DOCTYPE html>
<html>
    <head>
        <title>Boulogne Emploi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/imagehover.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/script.js" type="text/javascript"></script>
    </head>

    <body>

        <!--- header --->
        <?php include ("header.php"); ?>
        <header id="header-about"></header>
        <!--- header --->
        <div class="col s12 white center-align" style="padding: 15px;">
            <!--            <marquee loop="1">Découvrez notre nouvelle structure! Fusion de Réussir-Ensemble, Boulogne-Emploi et Mission-Locale</marquee>-->
            ok
        </div>

        <!---   --->
        <div class="row" id="about-site">
            <div class="container">
                <div class="col s12">
                    <div class="col s12 white about">
                        <span class="col s12 grey-text text-darken-1 title center-align">QUI SOMMES-NOUS</span>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                        <br><br>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="col s12 test about center-align">
                        <i class="fa-3x fa fa-building-o"></i>
                        <h5>RÉUSSIR-ENSEMBLE</h5>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="col s12 test1 about center-align">
                        <i class="fa-3x fa fa-building-o"></i>
                        <h5>BOULOGNE-EMPLOI</h5>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="col s12 test about center-align">
                        <i class="fa-3x fa fa-building-o"></i>
                        <h5>MISSION LOCALE</h5>
                        <p>Crebriores quorum potuit emendabat populari praeteritis domesticus auctique translationem angorem discerpti animi super Aquitania advenit populari Gallus rettulimus maerens auctique quam translationem prohibebant imbres protector Constantinopolim opperiens angorem super maerens.</p>
                    </div>
                </div>
            </div>
        </div>


        <?php include "footer.php"; ?>
    </body>

</html>
