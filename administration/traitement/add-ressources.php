<?php

require 'class/data.class.php';

$pdo = new data();


if (!empty($_POST)) {

    $titre = htmlspecialchars($_POST['title_ressources']);
    $description = htmlspecialchars($_POST['description_ressources']);
    $contenu = $_POST['content_ressources'];
    $catego = htmlspecialchars($_POST['cat_ressources']);

    $picture = $_FILES['picture'];
    $picture_name = $_FILES['picture']['name'];
    $picture_type = $_FILES['picture']['type'];
    $picture_size = $_FILES['picture']['size'];
    $picture_extension = strrchr($picture_name, ".");
    $good_extension = array('.jpg', '.jpeg', '.png', '.gif', '.JPG', '.JPEG', '.PNG', '.GIF');
    $picture_tmp_name = $_FILES['picture']['tmp_name'];
    $picture_url = 'galleryRessource/'.$picture_name;


    if (!empty($titre) AND !empty($description) AND !empty($contenu) AND !empty($catego)) {



            if ($picture_size < 5000000) {

                if (in_array($picture_extension, $good_extension)) {



                move_uploaded_file($picture_tmp_name, $picture_url);



        $pdo->insertRessource($titre, $description, $contenu, $catego, $picture_url);

        header("Location: ../home.php");

                }else {
                    echo 'Extension non valide !';
                }
            }else {
                echo 'Fichier trop lourd !';
            }

    }else {
        echo 'Veuillez remplir tous les champs ';
    }


}
