<?php
if (isset($_POST['displayRessources'])) {
    /*
     * Redeclaration de l'objet datafront obligatoire lors d'un appel Ajax côté serveur
     */
    require 'class/Data.class.php';
    $pdo = new Data ();
    $check = $_POST['displayRessources'];
    $donnees = $pdo->afficheRessourcesIndexById($check);
} else {
    $donnees = $pdo->afficheRessourcesIndex();
}
?>

<div class="ressources" style="padding: 0;">
    <?php
    foreach ($donnees as $response) {
        echo '
        <div class="col s12 m3">
           <figure class="imghvr-slide-up img-ressources">
                <img src="administration/traitement/' . $response['picture_ressource'] . ' "/>
                <figcaption style="background-color: rgba(0,0,0, 0.7)">
                  ' . $response['title_ressource'] . ' <br>
                  ' . $response['description'] . '
                </figcaption>
                <a href="ressources.php?ressource=' . $response['id_ressources'] . '"></a>
           </figure>
        </div>
        ';
    }
    ?>
</div>
