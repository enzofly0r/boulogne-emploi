-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2016 at 12:07 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `boulogneemploi`
--

-- --------------------------------------------------------

--
-- Table structure for table `be_actualites`
--

CREATE TABLE IF NOT EXISTS `be_actualites` (
  `id_actus` int(11) NOT NULL AUTO_INCREMENT,
  `actus_cat_id` int(11) NOT NULL,
  `title_actu` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `content_actu` text NOT NULL,
  `picture_url` text NOT NULL,
  `users_id` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `importante` int(11) NOT NULL,
  `rating_heart` int(11) NOT NULL,
  `activate` int(11) NOT NULL,
  PRIMARY KEY (`id_actus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `be_actualites`
--

INSERT INTO `be_actualites` (`id_actus`, `actus_cat_id`, `title_actu`, `description`, `content_actu`, `picture_url`, `users_id`, `date`, `importante`, `rating_heart`, `activate`) VALUES
(50, 4, 'fgdfsgnhgerh', 'erhqthrgerqn', '<p>rhqeeshez&lt;gerhthr</p>', 'galleryActu/debian-wallpapers-full-hd-wallpaper-search-page-2.jpg', 'Adrien ', '0000-00-00', 0, 1, 0),
(55, 4, 'fufufufuf', 'uffufufufuf', '<p>fufufufufufu</p>', 'galleryActu/0d227637cc9b514e5c9b943f10cd8089_large.jpeg', 'Adrien ', '0000-00-00', 0, 0, 0),
(56, 4, 'vvcvcvcvcv', 'cvcvcvcvcv', '<p>vvcvcvccvcv</p>', 'galleryActu/wallpaper-2710336.png', 'Adrien ', '0000-00-00', 0, 5, 0),
(57, 4, 'sdfgrhqerh', 'dhstrjhtrj', '<p>strjrj</p>', 'galleryActu/ceystalhorizon.png', 'Adrien ', '0000-00-00', 0, 3, 0),
(58, 4, '&lt;sdgqgrg', 'rqghqerh', '<p>wdrhqeh</p>', 'galleryActu/3S0NkqV.jpg', 'Adrien ', '0000-00-00', 0, 3, 0),
(60, 4, '&lt;srrqqeh', 'qrhqrezh', '<p>rqzh</p>', 'galleryActu/ceystalhorizon.png', 'Adrien ', '0000-00-00', 0, 4, 0),
(61, 4, 'rqehqh', 'qerhqh', '<p>qerheqh</p>', 'galleryActu/3S0NkqV.jpg', 'Adrien ', '0000-00-00', 0, 0, 0),
(62, 2, 'Final Test ', 'Test Finale pour l''upload d''images', '<p>sghqrhqet</p>', 'galleryActu/0d227637cc9b514e5c9b943f10cd8089_large.jpeg', 'Adrien ', '0000-00-00', 0, 0, 0),
(65, 2, 'zqgqzrh', 'qrehrqehh', '<p>qerhqerrh</p>', 'galleryActu/3S0NkqV.jpg', 'Adrien ', '0000-00-00', 0, 0, 0),
(67, 5, 'strrnr', 'srtnrt', '<p>strjsrtj</p>', 'galleryActu/0d227637cc9b514e5c9b943f10cd8089_large.jpeg', 'Adrien ', '0000-00-00', 0, 0, 0),
(68, 3, 'qbrebr', 'qrebrb', '<p>qrebeberb</p>', 'galleryActu/3S0NkqV.jpg', 'Adrien ', '0000-00-00', 0, 0, 0),
(75, 4, 'wgsgn', 'srtrt', '<p>srytjnrztjn</p>', 'galleryActu/hd_3d_wallpapers_wallpaper_357_image_desktop96.jpg', 'Adrien ', '0000-00-00', 0, 0, 0),
(77, 3, 'qezgzqg', '&lt;sdgqg', '<p>qsezHH</p>', 'galleryActu/debian-wallpapers-full-hd-wallpaper-search-page-2.jpg', 'Adrien ', '0000-00-00', 0, 0, 0),
(80, 1, 'LAST ACTUALITÉeee', 'Actualites 8000', '<p>Actualites 8000&nbsp;Actualites 8000&nbsp;Actualites 8000Actualites 8000Actualites 8000Actualites 8000Actualites 8000&nbsp;Actualites 8000Actualites 8000&nbsp;Actualites 8000</p>\r\n<p>Actualites <strong>8000Actualites 8000&nbsp;Actualites</strong> 8000Actualites 8000Actualites 8000&nbsp;Actualites 8000</p>\r\n<p>Actualites 8000Actualites 8000&nbsp;Actualites 8000Actualites 8000Actualites 8000&nbsp;Actualites 8000Actualites 8000Actualites 8000&nbsp;Actualites 8000</p>\r\n<p>Actualites 8000Actualites 8000&nbsp;Actualites 8000Actualites 8000Actualites 8000&nbsp;Actualites 8000</p>', 'galleryActu/code_by_mercurytuts-d8mtku5.jpg', 'maxime ', '0000-00-00', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `be_actualites_cat`
--

CREATE TABLE IF NOT EXISTS `be_actualites_cat` (
  `id_cat_actus` int(11) NOT NULL AUTO_INCREMENT,
  `title_cat_actus` varchar(250) NOT NULL,
  `icon_url` text NOT NULL,
  PRIMARY KEY (`id_cat_actus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `be_actualites_cat`
--

INSERT INTO `be_actualites_cat` (`id_cat_actus`, `title_cat_actus`, `icon_url`) VALUES
(1, 'Je recherche un emploi', 'work'),
(2, 'Créer son activité', 'vpn_key'),
(3, 'Me former', 'polymer'),
(4, 'Être accompagné', 'supervisor_account'),
(5, 'M''orienter et connaitre les métiers', 'my_location'),
(6, 'Services pour entreprise et employeurs', 'business'),
(7, 'Autres conseils et services', 'perm_device_information');

-- --------------------------------------------------------

--
-- Table structure for table `be_rank`
--

CREATE TABLE IF NOT EXISTS `be_rank` (
  `id_rank` int(11) NOT NULL AUTO_INCREMENT,
  `rank` varchar(250) NOT NULL,
  PRIMARY KEY (`id_rank`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `be_rank`
--

INSERT INTO `be_rank` (`id_rank`, `rank`) VALUES
(1, 'Administrateur'),
(2, 'Super Administrateur');

-- --------------------------------------------------------

--
-- Table structure for table `be_ressources`
--

CREATE TABLE IF NOT EXISTS `be_ressources` (
  `id_ressources` int(11) NOT NULL AUTO_INCREMENT,
  `ressources_cat_id` int(11) NOT NULL,
  `title_ressource` varchar(250) NOT NULL,
  `picture_ressource` text NOT NULL,
  `description` varchar(150) NOT NULL,
  `content_ressource` text NOT NULL,
  `users_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `activate` int(11) NOT NULL,
  PRIMARY KEY (`id_ressources`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `be_ressources`
--

INSERT INTO `be_ressources` (`id_ressources`, `ressources_cat_id`, `title_ressource`, `picture_ressource`, `description`, `content_ressource`, `users_id`, `date`, `activate`) VALUES
(12, 1, 'Premiere ressources', 'galleryRessource/hd_3d_wallpapers_wallpaper_357_image_desktop96.jpg', 'Ressource du siecle', '&lt;p&gt;Ressource du siecle Ressource du siecle Ressource du siecle Ressource du siecle Ressource du siecle vRessource du siecle Ressource du siecle Ressource du siecle Ressource du siecle Ressource du siecle Ressource du siecle Ressource du siecle&lt;/p&gt;', 0, '0000-00-00', 0),
(13, 1, 'Ressource 2', 'galleryRessource/debian-wallpapers-full-hd-wallpaper-search-page-2.jpg', 'Ressource du siecle 2', '&lt;p&gt;Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2 Ressource 2&lt;/p&gt;', 0, '0000-00-00', 0),
(14, 3, 'Ressource 3', 'galleryRessource/3S0NkqV.jpg', 'c', '&lt;p&gt;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3&lt;/p&gt;', 0, '0000-00-00', 0),
(15, 3, 'Ressource 3', 'galleryRessource/0d227637cc9b514e5c9b943f10cd8089_large.jpeg', 'Ressource du siecle 3', '&lt;p&gt;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3&amp;nbsp;Ressource du siecle 3Ressource du siecle 3Ressource du siecle 3&lt;/p&gt;', 0, '0000-00-00', 0),
(17, 1, 'Ressource 5', 'galleryRessource/wallpaper-2710336.png', 'Ressource 5', '&lt;p&gt;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource5&lt;/p&gt;', 0, '0000-00-00', 0),
(18, 1, 'Ressource 6', 'galleryRessource/debian-wallpapers-full-hd-wallpaper-search-page-2.jpg', 'Ressource 6', '&lt;p&gt;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5&amp;nbsp;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5&amp;nbsp;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5&lt;/p&gt;', 0, '0000-00-00', 0),
(19, 5, 'Ressource 7', 'galleryRessource/0d227637cc9b514e5c9b943f10cd8089_large.jpeg', 'Ressource 7', '&lt;p&gt;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5&amp;nbsp;Ressource 5Ressource 5&amp;nbsp;Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5Ressource 5&amp;nbsp;Ressource 5&lt;/p&gt;', 0, '0000-00-00', 0),
(20, 4, 'Final', 'galleryRessource/234678.jpg', 'final', '&lt;p&gt;final test best of the world blablablabalablabla !!!!!!!!&lt;/p&gt;', 0, '0000-00-00', 0),
(21, 1, 'Ressource 9', 'galleryRessource/code_by_mercurytuts-d8mtku5.jpg', 'Ressource 9', '&lt;p&gt;Ressource 9&amp;nbsp;Ressource 9Ressource 9&amp;nbsp;Ressource 9&amp;nbsp;Ressource 9Ressource 9&amp;nbsp;Ressource 9&amp;nbsp;Ressource 9 &amp;nbsp;Ressource 9 &amp;nbsp;Ressource 9&amp;nbsp;Ressource 9&lt;/p&gt;', 0, '0000-00-00', 0),
(26, 1, 'Ressource 10', 'galleryRessource/code_by_mercurytuts-d8mtku5.jpg', 'Ressource 10', '&lt;p&gt;Ressource 10 Ressource 10 Ressource 10 Ressource 10 Ressource 10&amp;nbsp;&lt;/p&gt;', 0, '0000-00-00', 0),
(27, 3, 'Ressource 13', 'galleryRessource/tools_artwork_flat_job_david_l_2560x1600_wallpaperno.com.jpg', 'Ressource 13', '&lt;p&gt;Ressource 10 Ressource 10 Ressource 10 Ressource 10 Ressource 10&lt;/p&gt;', 0, '0000-00-00', 0),
(28, 1, 'aaaaaaaaaaaaaaaa', 'galleryRessource/ice-background-14140151326az.jpg', 'aaaaaaaaaaaaaaa', '&lt;p&gt;aaaaaaaaaaaaaa&lt;/p&gt;', 0, '0000-00-00', 0),
(29, 3, 'Ressource 100000', 'galleryRessource/ice-background-14140151326az.jpg', 'Ressource 100000', '&lt;p&gt;aaaaaaaaaaaaaaaaaaaaaa&lt;/p&gt;', 0, '0000-00-00', 0),
(33, 1, '1234556', 'galleryRessource/234678.jpg', '1111111111111', '<p>enzo esteves</p>', 0, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `be_ressources_cat`
--

CREATE TABLE IF NOT EXISTS `be_ressources_cat` (
  `id_cat_ressources` int(11) NOT NULL AUTO_INCREMENT,
  `title_cat_ressources` varchar(250) NOT NULL,
  `icon_url` text NOT NULL,
  PRIMARY KEY (`id_cat_ressources`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `be_ressources_cat`
--

INSERT INTO `be_ressources_cat` (`id_cat_ressources`, `title_cat_ressources`, `icon_url`) VALUES
(1, 'EMPLOI', 'work'),
(2, 'ENTREPRENDRE', 'vpn_key'),
(3, 'FORMATION', 'polymer'),
(4, 'ACCOMPAGNEMENT', 'supervisor_account'),
(5, 'ORIENTATION', 'my_location'),
(6, 'SERVICE PRO', 'business'),
(7, 'AUTRES', 'perm_device_information');

-- --------------------------------------------------------

--
-- Table structure for table `be_users`
--

CREATE TABLE IF NOT EXISTS `be_users` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `rank` int(11) NOT NULL,
  `picture_url` text NOT NULL,
  `workplace` varchar(250) NOT NULL,
  `status` varchar(250) NOT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `be_users`
--

INSERT INTO `be_users` (`id_users`, `name`, `lastname`, `email`, `password`, `rank`, `picture_url`, `workplace`, `status`) VALUES
(1, 'Simplon', 'Simplon', 'eesteves@hotmail.fr', 'codeurKiFFeur', 2, 'https://pbs.twimg.com/profile_images/651694588449656832/BbFV6m9v.png', 'Simplon', 'Developpeur Junior'),
(34, 'czerwinski', 'Adrien', 'adrien-marine@hotmail.com', '$2y$10$9yPUriUMoHyQ9fu.QQD1duyJNbW8hn77kCfkuXKvLdy3dO8d1ZK3K', 2, 'http://www.etremarin.fr/sites/default/files/pmm_ceremonie_medium2.jpg', 'simplon', 'pdg'),
(36, 'senecal', 'maxime', 'msenecal@simplon.co', '$2y$10$fkcgT6qkmslWHrQrPNT8buVXRx1.8pvAgmuVoYs8fZhUpCyFJQ336', 2, 'https://media.giphy.com/media/HJ4Eaags5sXBK/giphy.gif', 'simplon', 'formateur'),
(37, 'Enzo', 'Enzo', 'Enzo', 'Enzo', 1, '', 'dede', 'edede'),
(38, 'Esteves', 'Enzo', 'eesteves@hotmail.fr', 'lolmdr', 1, '', 'Simplon', 'Developpeur Junior'),
(39, 'Admont', 'Cécile ', 'cecile@simplon.co', '$2y$10$Vblyf29fQbJ7diNuHsRBuOBS3LUf0aXTs5SFTBFeCMMYJAGUuqZPG', 1, '', 'réussir ensemble', 'sxx'),
(40, 'adri', 'adri', 'adrikiffmarine@gmail.com', '$2y$10$0zWd1oX3eXpvxU3cihGeE.PJxHf0pm4gjllBRshaU0JqnzlMXhWOm', 2, '', 'marine', 'marinier'),
(41, 'img', 'img', 'img@hotmail.com', '$2y$10$6E5jpNaAvFG68E.VdyrNJugB2T9OvW9/CzLtZFVJRAVZw5TaExJ.e', 2, 'galleryUser/', 'img', 'img'),
(42, 'testimg', 'testimg', 'testimg@hotmi.com', '$2y$10$cAnZJCSelyeR6cdvDQPp3.ZGON13lJZy/b65WYzH2pRUv/0YJ1VQ2', 2, 'galleryUser/', 'testimg', 'testimg'),
(43, 'imgtest', 'imgtest', 'imgtest@hotmail.com', '$2y$10$rRZ2uUZca3XU1ilHuyObaO5yM7YKUwcq12OTHQ1aIVNOMCulIup5q', 2, 'galleryUser/', 'imgtest', 'imgtest'),
(44, 'aaaaaaaa', 'aaaaa', 'aaaaaaaa@htomail.com', '$2y$10$xGMRDV/KdeLjObzdTwiJguY7tFe4AoL9U3SKxUOyWW6FkJDqWyaye', 2, 'galleryUser/C', 'aaaa', 'aaaa'),
(45, 'hhhhhh', 'hhh', 'hhhh@hotmail.com', '$2y$10$HHEGZeonf9MHwf8zbRIf0OOSlb7aDznvBPsxs.VDkVOLmM6nUPa/.', 2, 'galleryUser/', 'hhh', 'hhh'),
(46, 'final', 'final', 'final@hotmail.com', '$2y$10$2vFYrFhpuEmesQT3qDXZLeRspijtkIL.k0UJnJSOuFlqnn0T9BjfS', 2, '', 'final', 'final'),
(47, 'aaaaaaaaaaaaa', 'aaaaaaaaaa', 'aaaaa@hotmail.com', '$2y$10$7X0rHGsIWBl6byiLHLtd6.LvbWGKvaqAM.CHHkFE8JBNgQHjR9G5O', 2, '', 'aaaaaaaaaa', 'aaaaaaaaaaaaa'),
(48, 'lulu', 'lulu', 'lulu@lulu.lu', '$2y$10$26cUcpUjN7y3nM.Xu0jYK.YHVo2McuDDw9F1NXKuBP7gC7pvGjbAK', 1, '', 'lulu', 'lulu');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
