<?php
session_start();
if (empty($_SESSION)) {
    die('404');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom-adm.css">
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <script>tinymce.init({selector: 'textarea'});</script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"  integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/administration.js" type="text/javascript"></script>
    </head>

    <body>

        <?php include("header.php"); ?>

        <?php
        include("edit-profil.php");

        $valueActu = $pdo->afficheActuId($_GET['idactu']);
        ?>
        <form action="traitement/updateActus.php?idactu=<?php echo $_GET['idactu']; ?>" method="POST" enctype="multipart/form-data">
            <div class="row" id="form-add-actus">
                <div class="col s12 green-custom flow-text white-text center-align" style="padding: 1vh;">Modifier une actualitée</div>
                <div class="col s12 m10  offset-m1">
                    <div class="input-field col s12 m6">
                        <input value="<?php echo $valueActu->title_actu; ?>" id="title_actus" name="title_actus" type="text" class="validate">

                    </div>
                    <div class="input-field col s12 m6">
                        <input value="<?php echo $valueActu->description; ?>" id="description_actus" name="description_actus" type="text" class="validate">

                    </div>
                    <div class="input-field col s12 m6">
                        <select id="cat_actus" name="cat_actus">
                            <?php
                            $reponse = $pdo->afficheActu();
                            foreach ($reponse as $donnees) {
                                echo '<option value="' . $donnees['id_cat_actus'] . ' ">' . $donnees['title_cat_actus'] . '</option>';
                            }
                            ?>
                        </select>
                        <label>Selectionner une catégorie</label>
                    </div>
                    <div class="col s12 m6">
                        <img style="width: 140px;height: 70px;float: left;" src="traitement/<?php echo $valueActu->picture_url; ?>" alt="" />
                        <input style="float: left;" type="file" name="picture" id="fileToUpload">
                    </div>
                    <div class="col s12">
                        <textarea id="content_actu" name="content_actu"><?php echo $valueActu->content_actu; ?></textarea>
                    </div>

                    <div class="col s12">
                        <button type="submit" class="btn waves-effect waves-light blue-grey darken-1 right">Modifier
                            <i class="material-icons right">send</i>
                        </button>
                        </form>
                    </div>
                </div>
            </div>

    </body>

</html>
