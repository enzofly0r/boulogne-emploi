<?php

require 'ident.php';

class Data {

    /** PROPRIÉTÉS * */
    /*
     * Données de connexion à la base de donnée
     */
    private $_host = DB_HOST;
    private $_user = DB_USER;
    private $_pass = DB_PASS;
    private $_pdo;

    /** tables membres * */
    private $_tableMembres;

    /** MÉTHODES * */
    /*
     * Instentation de la connexion à la base de donnée
     */
    public function __construct() {
        $this->_pdo = new PDO($this->_host, $this->_user, $this->_pass);
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_tableMembres = "be_users";
    }

    /*
     * Affiche les actus navigation rapide
     */

    public function afficheRapideActus() {
        $req = $this->_pdo->prepare("SELECT * FROM be_actualites LEFT OUTER JOIN be_actualites_cat ON be_actualites.actus_cat_id = be_actualites_cat.id_cat_actus ORDER BY rating_heart DESC LIMIT 7");
        $req->execute();
        return $req->fetchAll();
    }

    /*
     * Affiche toutes les actualités
     */

    public function afficheAllActus() {
        $req = $this->_pdo->prepare("SELECT * FROM be_actualites ORDER BY id_actus DESC");
        $req->execute();
        return $req->fetchAll();
    }

    /*
     * Affiche une actu via un ID renvoyé
     */

    public function afficheActusById() {
        $req = $this->_pdo->prepare('SELECT * FROM be_actualites WHERE id_actus=' . $_GET['idactu'] . ' ');
        $req->execute();
        return $req->fetchAll();
    }

    public function afficheHeaderActus() {
        $req = $this->_pdo->prepare('SELECT * FROM be_actualites ORDER BY rating_heart DESC LIMIT 5');
        $req->execute();
        return $req->fetchAll();
    }

    /*
     * Déclenche un +1 à l'article
     */

    public function ratingHeart($id) {
        $req = $this->_pdo->prepare('UPDATE be_actualites SET rating_heart=rating_heart+1 WHERE id_actus=' . $id . ' ');
        $req->execute();
    }

    /*
     * Affiche les 6 dernières ressources postés sur l'index
     */

    public function afficheRessourcesIndex() {
        $test = 'SELECT * FROM be_ressources ORDER BY id_ressources DESC LIMIT 6';
        $req = $this->_pdo->prepare($test);
        $req->execute();
        return $req->fetchAll();
    }

    /*
     * Requête déclenché par l'ajax pour affiché des ressources via un id
     */

    public function afficheRessourcesIndexById($check) {
        $test = 'SELECT * FROM be_ressources WHERE ressources_cat_id =' . $check . ' ';
        $req = $this->_pdo->prepare($test);
        $req->execute();
        return $req->fetchAll();
    }

    /*
     * Affiche toutes les catégories des ressources
     */

    public function afficheAllCategorieRessources() {
        $req = $this->_pdo->prepare('SELECT * FROM be_ressources_cat');
        $req->execute();
        return $req->fetchAll();
    }

    // AFFICHAGE NAVIGATION RESSOURCES
    public function ressourceAffiche() {
        $req = $this->_pdo->prepare("SELECT * FROM be_ressources ORDER BY id_ressources DESC LIMIT 10");
        $req->execute();
        return $req->fetchAll();
    }

    public function afficheRessourcesById() {
        $req = $this->_pdo->prepare('SELECT * FROM be_ressources WHERE id_ressources=' . $_GET['ressource'] . ' ');
        $req->execute();
        return $req->fetchAll();
    }

    // ENVOI DE MAIL
    public function email($to, $subject, $message, $headers) {
        mail($to, $subject, $message, $headers);
    }

}
