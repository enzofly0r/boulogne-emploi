<!DOCTYPE html>
<html>
    <head>
        <title>Oopss.. !</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom-adm.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"  integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/administration.js" type="text/javascript"></script>
    </head>

    <body id="bodyError">

        <div class="row">
            <div class="col s12" id="layerError">
                <div class="container white-text">
                    <div class="col s12 m12 flow-text center-align" style="margin-top: 20vh;">
                        <h2>ERREUR!<br/> IDENTIFIANT OU MOT DE PASSE INCORRECT!</h2>
                    </div>
                    <div class="col s6 offset-m3 m3">
                        <a href="index.php" class="btn z-depth-0 buttonError"><i class="material-icons right">lock</i>SE CONNECTER</a>                    
                    </div>
                    <div class="col s6 m3">
                        <a href="../index.php" class="btn z-depth-0 buttonError"><i class="material-icons right">dashboard</i>RETOUR AU SITE</a>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>
