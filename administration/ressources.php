<div class="container">
  <div class="row">
    <div class="col s12 m12">
      <a href="gest-ressources.php">
        <div class="card add-ressources">
          <i class="medium material-icons">library_add</i>
        </div>
      </a>
    </div>
    <div class="ressources-content">
      <?php
      $reponse = $bdd->query('SELECT * FROM be_ressources ORDER BY id_ressources DESC');
      while ($donnees = $reponse->fetch()) {
        $description = $donnees['description'];
        echo '
        <div class="col s12 m3">
        <div class="card grey lighten-3">

        <div class="card-content">

        <span class="card-title">' . $donnees['title_ressource'] . '</span>
        <img style="width: 100%;height: 100px;object-fit: cover;" src="traitement/' . $donnees['picture_ressource'] . '">
        <p style="height: 70px;">' . substr($description, 0, 80) . '...</p>
        </div>
        <div class="card-action">
        <a href="#" class="delete-ressources" id="' . $donnees['id_ressources'] . '"><i class="small material-icons">delete</i></a>
        <a href="update-ressources.php?idRessource=' . $donnees['id_ressources'] . '"><i class="small material-icons">edit</i></a>
        </div>
        </div>
        </div>
        ';
      }
      $reponse->closeCursor(); // Termine le traitement de la requête
      ?>
    </div>
  </div>
</div>
