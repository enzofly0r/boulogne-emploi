<!DOCTYPE html>
<html>
    <head>
        <title>Boulogne Emploi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/imagehover.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/script.js" type="text/javascript"></script>
    </head>

    <body>

        <!--- header --->
        <?php include ("header.php"); ?>
        <header id="header-actualites"></header>
        <div class="col s12 white" style="padding: 10px;">
            <a href="actualites.php" class="btn-flat">Retour <i class="fa fa-reply" aria-hidden="true" style="font-size: 15px;"></i></a>
        </div>
        <!--- header --->

        <!---   --->
        <div class="row" id="actualites-site">
            <div class="container">
                <!-- actualites all -->
                <?php
                if (!isset($_GET['idactu'])) { // Affiche toutes les actualités si le GET N'est pas définie
                    $donnees = $pdo->afficheAllActus();
                    foreach ($donnees as $reponse) {
                        $title = $reponse['title_actu'];
                        $content = $reponse['content_actu'];
                        echo '
          <div class="col s12 m6">
          <div class="col s12 white actualites" >
          <div class="col s12 m4 picture-actualites">
          <img src="administration/traitement/' . $reponse['picture_url'] . ' "/>
          </div>
          <div class="col s12 m8">
          <span class="col s12 grey-text text-darken-1 title center-align">' . strtoupper($title) . '</span>
          ' . substr($content, 0, 135) . '...
          </div>
          <a href="actualites.php?idactu=' . $reponse['id_actus'] . '" class="waves-effect waves-teal btn-flat right">Lire +</a>
          </div>
          </div>
          ';
                    }
                    ?>
                    <!-- actualites all -->
                    <!--- actu détails -->
                    <?php
                } else { // Affiche une actualité définie par l'id dans le GET
                    ?>
                    <div class="col s12 m8">
                        <div class="col s12 white actualites">
                            <?php
                            $donnees = $pdo->afficheActusById();
                            foreach ($donnees as $reponse) {
                                $title = $reponse['title_actu'];
                                echo '
              <span class="col s12 grey-text text-darken-1 title center-align">' . strtoupper($title) . '</span>
              <div class="col s12" id="actualites-details">
              <img style="height: 270px;" src="administration/traitement/' . $reponse['picture_url'] . ' "/>
              <div class="col s12 grey-text text-darken-1 no-padding"> ' . $reponse['description'] . '<br></div>
              ' . $reponse['content_actu'] . '
              <div class="col s6 grey-text text-darken-1 no-padding">' . $reponse['date'] . '</div>
              <div class="col s6 right-align grey-text no-padding">';
                                if (!isset($_COOKIE['rating' . $reponse['id_actus']])) {
                                    echo '<i id="' . $reponse['id_actus'] . '"class="ratingHeart fa fa-lg fa-heart red-text"></i></div>';
                                } else {
                                    echo'<i id="' . $reponse['id_actus'] . '"class="ratingHeart fa fa-lg fa-heart"></i></div>';
                                }
                                echo'</div>';
                            }
                            ?>

                        </div>
                    </div>
                    <!--- recent post --->
                    <div class="col s12 m4">
                        <div class="col s12 white" id="recent-post">
                            <!-- title div box -->
                            <span class="col s12 grey-text text-darken-1 title center-align">DERNIÈRE ACTUALITÉS</span>
                            <!-- post -->
                            <?php include "navigation_actualites.php" ?>
                            <!-- post -->
                        </div>
                    </div>
                    <!--- recent post --->
                    <?php
                }
                ?>
                <!--- actu détails -->
            </div>
        </div>
        <?php include "footer.php"; ?>
    </body>

</html>
