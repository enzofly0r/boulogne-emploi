<?php
require 'traitement/class/data.class.php';

$pdo = new data();
?>
<nav class="green-custom">
    <a href="home.php" class="left hide-on-med-and-down" id="logo-adm">
        Administration
    </a>
    <ul class="right hide-on-med-and-down">
        <li><a class="modal-trigger" href="#modal1"><i class="material-icons">person_pin</i></a></li>
        <li><a href="../index.php" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Retourner au site" onclick="Materialize.toast('Retour au site dans 2s..', 2000, '', function () {
                    window.location.replace('#')
                })"><i class="material-icons">fast_rewind</i></a></li>
        <li><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Déconnexion" href="traitement/deconnexion.php"><i class="material-icons">power_settings_new</i></a></li>
    </ul>
    <ul id="slide-out" class="side-nav">
        <li><a class="modal-trigger" href="#modal1"><i class="material-icons">person_pin</i></a></li>
        <li><a href="../index.php" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Retourner au site" onclick="Materialize.toast('Retour au site dans 2s..', 2000, '', function () {
                    window.location.replace('#')
                })"><i class="material-icons">fast_rewind</i></a></li>
        <li><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Déconnexion" href="traitement/deconnexion.php" ><i class="material-icons">power_settings_new</i></a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
</nav>
<div class="header-bottom flow-text">
    <div id="message">Bienvenue <?php echo $_SESSION['lastname'] . ' ' . $_SESSION['firstname']; ?></div>
</div>