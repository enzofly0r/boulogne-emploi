$(document).ready(function () {

    // Initialize collapse button
    $('.button-collapse').sideNav({// jquery burger menu
        menuWidth: 90, // Default is 240
        edge: 'right', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    }
    );

    $('.modal-trigger').leanModal({// jquery modal
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        in_duration: 300, // Transition in duration
        out_duration: 200// Transition out duration
                //ready: function() { alert('Ready'); }, // Callback for Modal open
                //complete: function() { alert('Closed'); } // Callback for Modal close
    }
    );

    $('ul.tabs').tabs('select_tab', 'tab_id'); // jquery tabs

    $('.tooltipped').tooltip({delay: 50}); // jquery tooltip
    $('select').material_select();

    /*
     * ajax changement de photo index
     */
    $('body').on("focus", "#password", function () { //keypress
        var displayPicture = document.getElementById("email").value;
        $.ajax({
            url: 'traitement/affichePhotoConnect.php',
            type: 'POST',
            data: {displayPicture: displayPicture},
            dataType: 'html',
            success: function ($response) {
                $(".profil-picture").empty();
                $(".profil-picture").append($response);
                //$($response).appendTo(".ressources");
            }
        });
    });

    /*
     * ajax add utilisateurs
     */
    $("#btn-add-user").click(function () {
        var lastname = document.getElementById("lastname").value;
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var rank = document.getElementById("rank").value;
        var workplace = document.getElementById("workplace").value;
        var status = document.getElementById("status").value;

        $.ajax({
            url: 'traitement/add-user.php',
            type: 'POST',
            data: {lastname: lastname, name: name, email: email, password: password, rank: rank, workplace: workplace, status: status},
            dataType: 'html',
            success: function () { // code_html contient le HTML renvoyé
                $("#users").load(location.href + " #users");
                $("#message").text("Utilisateur ajoutés avec succès.");
                $("#message").css({
                    "color": "green",
                    "fontWeight": "400"
                });
            },
            error: function () {
                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
                $("#message").css({
                    "color": "red",
                    "fontWeight": "400"
                });
            },
            done: function () {
                $("#lastname").val("");
                $("#name").val("");
                $("#email").val("");
                $("#password").val("");
                $("#rank").val("");
                $("#workplace").val("");
                $("#status").val("");
            }
        });
    });

    /*
     * ajax add actualites
     */
//    $("#btn-add-actus").click(function () {
//        var title_actus = document.getElementById("title_actus").value;
//        var description_actus = document.getElementById("description_actus").value;
//        var cat_actus = document.getElementById("cat_actus").value;
//        var content_actu = tinyMCE.get('content_actu').getContent();
//        var importante = document.getElementById("importante").checked;
//        var picture = $("#fileToUpload").prop('files')[0].name;
//        console.dir(picture);
//        $.ajax({
//            url: 'traitement/add-actus.php',
//            type: 'POST',
//            data: {title_actus: title_actus, description_actus: description_actus, cat_actus: cat_actus, importante: importante, content_actu: content_actu, picture: picture},
//            dataType: 'html',
//            success: function () { // code_html contient le HTML renvoyé
//                $("#actus").load(location.href + " #actus");
//                $("#message").text("Actualité ajoutés avec succès.");
//                $("#message").css({
//                    "color": "green",
//                    "fontWeight": "400"
//                });
//                $("#title_actus").val("");
//                $("#description_actus").val("");
//            },
//            error: function () {
//                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
//                $("#message").css({
//                    "color": "red",
//                    "fontWeight": "400"
//                });
//            }
//        });
//    });

    /*
     * ajax add ressources
     */
    $("#btn-add-ressources").click(function () {
        var title_ressources = document.getElementById("title_ressources").value;
        var description_ressources = document.getElementById("description_ressources").value;
        var cat_ressources = document.getElementById("cat_ressources").value;
        //content_actu = document.getElementById("content_actu").value;
        var content_ressources = tinyMCE.get('content_ressources').getContent();
        $.ajax({
            url: 'traitement/add-ressources.php',
            type: 'POST',
            data: {title_ressources: title_ressources, description_ressources: description_ressources, cat_ressources: cat_ressources, content_ressources: content_ressources},
            dataType: 'html',
            success: function () { // code_html contient le HTML renvoyé
                $("#ressources").load(location.href + " #ressources");
                $("#message").text("Ressource ajoutés avec succès.");
                $("#message").css({
                    "color": "green",
                    "fontWeight": "400"
                });
                $("#title_ressources").val("");
                $("#description_ressources").val("");
            },
            error: function () {
                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
                $("#message").css({
                    "color": "red",
                    "fontWeight": "400"
                });
            }
        });
    });

    /*
     * ajax suppression actualites
     */
    $('body').on("click", ".delete-actu", function () {
        deleteactus = $(this).attr('id');
        $.ajax({
            url: 'traitement/delete-actus.php',
            type: 'POST',
            data: {deleteactus: deleteactus},
            dataType: 'html',
            success: function () {
                $("#actus-content").load(location.href + " #actus-content");
                $("#message").text("Actualitée supprimés avec succès.");
                $("#message").css({
                    "color": "green",
                    "fontWeight": "400"
                });
            },
            error: function () {
                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
                $("#message").css({
                    "color": "red",
                    "fontWeight": "400"
                });
            }
        });
    });

    /*
     * ajax suppression Ressources
     */
    $('body').on("click", ".delete-ressources", function () {
        deleteressources = $(this).attr('id');
        $.ajax({
            url: 'traitement/delete-ressources.php',
            type: 'POST',
            data: {deleteressources: deleteressources},
            dataType: 'html',
            success: function () {
                $(".ressources-content").load(location.href + " .ressources-content");
                $("#message").text("Ressource supprimés avec succès.");
                $("#message").css({
                    "color": "green",
                    "fontWeight": "400"
                });
            },
            error: function () {
                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
                $("#message").css({
                    "color": "red",
                    "fontWeight": "400"
                });
            }
        });
    });

    /*
     * ajax suppression Administrateurs
     */
    $('body').on("click", ".delete-users", function () {
        deleteAdmin = $(this).attr('id');
        $.ajax({
            url: 'traitement/delete-users.php',
            type: 'POST',
            data: {deleteAdmin: deleteAdmin},
            dataType: 'html',
            success: function () {
                $(".users-content").load(location.href + " .users-content");
                $("#message").text("Administrateur supprimés avec succès.");
                $("#message").css({
                    "color": "green",
                    "fontWeight": "400"
                });
            },
            error: function () {
                $("#message").text("Erreur. Veuillez réessayer ultérieurement.");
                $("#message").css({
                    "color": "red",
                    "fontWeight": "400"
                });
            }
        });
    });


    $('textarea.tinymce').tinymce({
    });

});
