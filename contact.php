<!DOCTYPE html>
<html>
    <head>
        <title>Boulogne Emploi</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/imagehover.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/script.js" type="text/javascript"></script>
    </head>

    <body>

        <!--- header --->
        <?php include ("header.php"); ?>
        <header>
            <iframe id="header-contact" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2525.7120812035864!2d1.6056018151984153!3d50.725280479514225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47dc2c441f86adef%3A0x4beb0184106fbac3!2s13+Rue+des+Carreaux%2C+62200+Boulogne-sur-Mer!5e0!3m2!1sfr!2sfr!4v1457538891133" frameborder="0" style="border:0" allowfullscreen></iframe>
        </header>
        <!--- header --->

        <!---   --->
        <div class="row" id="contact-site">
            <div class="container">
                <div class="col s12 m8">

                    <form method="POST" action="traitement/send.php" class="col s12 white contact" id="contact-form">
                        <span class="col s12 grey-text text-darken-1 title center-align">NOUS CONTACTER</span>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">account_circle</i>
                            <input name="firstName" id="last_name" type="text" class="validate">
                            <label for="last_name">First Name</label>
                        </div>
                        <div class="input-field col s6">
                            <input name="lastName" id="last_name" type="text" class="validate">
                            <label for="last_name">Last Name</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">email</i>
                            <input name="email" id="last_name" type="text" class="validate">
                            <label for="last_name">Adresse mail</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">phone</i>
                            <input name="telephone" id="last_name" type="text" class="validate">
                            <label for="last_name">Tel: 0x.xx.xx.xx.xx</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">assignment_late</i>
                            <input name="sujet" id="last_name" type="text" class="validate">
                            <label for="last_name">Sujet du message</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea name="message" id="icon_prefix2" class="materialize-textarea"></textarea>
                            <label for="icon_prefix2">Message</label>
                        </div>
                        <button class="btn waves-effect waves-light right" type="submit" name="action">Envoyer
                            <i class="material-icons right">send</i>
                        </button>
                </div>
                </form>
                <div class="col s12 m4">
                    <div class="col s12 white contact" id="contact-form">
                        <span class="col s12 grey-text text-darken-1 title center-align">COORDONNÉES</span>
                        <ul>
                            <li><i class="fa fa-phone" aria-hidden="true"></i> Tél: 06 34 22 50 19</li>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> Adresse: 19 rue pichard</li>
                            <li><i class="fa fa-envelope-o" aria-hidden="true"></i> E-mail: mail@contact.fr</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php include "footer.php"; ?>
    </body>

</html>
