<?php

session_start();
require 'class/data.class.php';
$pdo = new data();


/*
* fin de l'envoie
*/
if (!empty($_POST) AND !empty($_FILES)) {

  $ressourcesCat = htmlspecialchars($_POST['cat_ressources']);
  $ressourcesTitre = htmlspecialchars($_POST['title_ressources']);
  $description = htmlspecialchars($_POST['description_ressources']);
  $ressourcesContenu = ($_POST['content_ressources']);
  $picture = $_FILES['picture'];

  $picture_name = $_FILES['picture']['name'];
  $picture_type = $_FILES['picture']['type'];
  $picture_size = $_FILES['picture']['size'];
  $picture_extension = strrchr($picture_name, ".");
  $good_extension = array('.jpg', '.jpeg', '.png', '.gif', '.JPG', '.JPEG', '.PNG', '.GIF');
  $picture_tmp_name = $_FILES['picture']['tmp_name'];
  $picture_url = 'galleryRessource/'.$picture_name;

  $users_id = $_SESSION['lastname'].' '.$_SESSION['name'];

  if (!empty($ressourcesCat) AND ! empty($ressourcesTitre) AND ! empty($description) AND ! empty($ressourcesContenu)) {


    if (!empty($picture_name)) {



      if ($picture_size < 5000000) {

        if (in_array($picture_extension, $good_extension)) {



          move_uploaded_file($picture_tmp_name, $picture_url);

          $pdo->updateRessources($_GET['idRessource'], $ressourcesCat, $ressourcesTitre, $description, $ressourcesContenu, $picture_url);
          header('Location: ../home.php#actus');

        }else {
          echo 'Extension non valide';
        }
      }else {
        echo 'Le fichier est trop lourd';
      }
    }

$pdo->updateRessourcesNoImg($_GET['idRessource'], $ressourcesCat, $ressourcesTitre, $description, $ressourcesContenu);
header('Location: ../home.php');





    }else {
      echo 'Veuillez remplir tous les champs !';
    }
  }
