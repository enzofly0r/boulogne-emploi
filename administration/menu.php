<div class="row">
  <div class="col s12 m12 no-padding">
    <ul class="tabs green-custom">
      <li class="tab col s3 m3"><a href="#ressources">Ressources</a></li>
      <li class="tab col s3 m3"><a href="#actus">Actualités</a></li>
      <?php
      if ($_SESSION['rank'] >= 2) {
        ?>
        <li class="tab col s3 m3"><a href="#users">Gestions Utilisateurs</a></li>
        <?php
      }
      ?>
      <li class="tab col s3 m3"><a href="#newletters">Newletters</a></li>
    </ul>
  </div>
  <div id="ressources" class="col s12 m12 no-padding">
    <?php include("ressources.php"); ?>
  </div>

  <!--*********************** ACTUS ***********************-->
  <div id="actus" class="col m12">
    <?php include("actus.php"); ?>
  </div>
  <!--********************************************-->
  <?php
  if ($_SESSION['rank'] >= 2) {
    ?>
    <div id="users" class="col m12">
      <?php include("gest-users.php"); ?>
    </div>
    <?php
  }
  ?>

</div>
<div id="newletters" class="col m12">Test 4</div>
</div>
