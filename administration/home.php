<?php
session_start();


if (empty($_SESSION)) {
    header('Location: error.php');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/custom-adm.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"  integrity="sha256-xI/qyl9vpwWFOXz7+x/9WkG5j/SVnSw21viy8fWwbeE=" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
        <script src="js/administration.js" type="text/javascript"></script>
    </head>

    <body>

        <?php include("header.php"); ?>

        <?php include("edit-profil.php"); ?>

        <?php include("menu.php"); ?>


    </body>

</html>
