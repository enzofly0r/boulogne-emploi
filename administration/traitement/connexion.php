<?php

session_start();

////////////////////////// CLASSES //////////////////////////
require 'class/data.class.php';

////////////////////////// INSTANTIATIONS CLASSES //////////////////////////
$pdo = new data();

if (!empty($_POST)) {

    // PROTECTION INJECTIONS
    $mailconnect = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);

    // SI LES CHAMPS SONT VIDES
    if (!empty($mailconnect) AND ! empty($password)) {

        // FORMAT EMAIL
        if ($pdo->regexMail($mailconnect)) {

            // SI LE COMPTE EXISTE
            $verifAccount = $pdo->verifAccount($mailconnect, $password);

            if ($verifAccount) {

                // CREATION SESSIONS
                $pdo->connectSession($mailconnect);
                header('Location: ../home.php');
                
            } else {
                $pdo->errorCon("Identifiant ou mot de pass incorrect !");
            }
        } else {
            $pdo->errorCon("email incorrecte !");
        }
    } else {
        $pdo->errorCon("Veuillez remplir tous les champs !");
    }
}
