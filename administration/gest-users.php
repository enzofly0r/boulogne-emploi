<div class="container">
    <div class="row">
        <!-- add users -->
        <div class="col s12 m12">
            <div class="card add-user modal-trigger" href="#add-user">
                <i class="medium material-icons">library_add</i>
            </div>
        </div>
        <div id="add-user" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h4>Ajouter un administrateur<i class="right small material-icons">person_pin</i></h4>
                <div class="row grey-text">
                    <div class="input-field col s12 m6">
                        <input name="lastname" id="lastname" type="text">
                        <label for="lastname">Prénom</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input name="name" id="name" type="text" >
                        <label for="name">Nom</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input name="email" id="email" type="email">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input name="password" id="password" type="password">
                        <label for="password">Password</label>
                    </div>
                    <div class="input-field col s12">
                        <select id="rank" name="rank">
                            <?php
                            $reponse = $bdd->query('SELECT * FROM be_rank');
                            while ($donnees = $reponse->fetch()) {
                                echo '<option value="' . $donnees['id_rank'] . ' ">' . $donnees['rank'] . '</option>';
                            }
                            $reponse->closeCursor(); // Termine le traitement de la requête
                            ?>
                        </select>
                        <label>Grade de l'utilisateur</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input id="workplace" name="workplace" type="text">
                        <label for="workplace">Employeur</label>
                    </div>
                    <div class="input-field col s12 m6">
                        <input id="status" name="status" type="text">
                        <label for="status">Poste</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" id="btn-add-user" class="modal-action modal-close waves-effect waves-green btn-flat blue-grey darken-3 white-text">Ajouter l'utilisateur</a>

            </div>



        </div>
        <!-- users -->
        <div class="users-content">
            <?php
            $reponse = $pdo->afficheUser();
            foreach ($reponse as $donnees) {
                echo '
      <div class="col s12 m12 l6" id="test">
      <div class="card grey lighten-3">
      <div class="card-content">
      <div class="row">
      <div class="col s12 m1"> <!-- PROFIL AVATAR -->
      <div id="picture-edituser">
      <img src="' . $donnees['picture_url'] . '"/></div>
      </div>
      <div class="col s12 m11"><p class="flow-text">' . $donnees['name'] . ' ' . $donnees['lastname'] . '</p></div>
      </div>
      <div class="row">
      <div class="col s12 m9"><p style="font-size: 19px; font-weight: 300;">' . $donnees['email'] . '</span></div>
      <div class="col s12 m3">
      <a href="#"><i class="small material-icons right">edit</i></a>
      <a href="#" class="delete-users" id="' . $donnees['id_users'] . '"><i class="small material-icons right">delete</i></a>
      </div>
      </div>
      </div>
      </div>
      </div>';
            }
            ?>
        </div>
    </div>

</div>
