<section class="row" id="header-actus">
  <?php
  $i = 0;
  $donnees = $pdo->afficheHeaderActus();
  foreach ($donnees as $reponse) {
    if ($i === 0) {
      echo '
      <figure class="col s12 m6 imghvr-slide-left big-img">
      <span class="col s12 title-picture flow-text center-align">
      ' . $reponse['title_actu'] . '
      </span>

      <span class="heart-picture">
      ' . $reponse['rating_heart'] . '
      <i id="' . $reponse['id_actus'] . '" class="fa-lg fa fa-heart" aria-hidden="true"></i>
      </span>
      <img src="administration/traitement/' . $reponse['picture_url'] . ' ">
      <figcaption style="background-color: rgba(0,0,0, 0.7)">
      ' . $reponse['description'] . '
      </figcaption>
      <a href="actualites.php?idactu='.$reponse['id_actus'].'"></a>
      </figure>
      ';
    } else {
      echo '
      <figure class="col s12 m3 imghvr-slide-right small-img">
      <span class="col s12 title-picture flow-text center-align">
      ' . $reponse['title_actu'] . '
      </span>
      <div class="refreshHeart">
      <span class="heart-picture">
      ' . $reponse['rating_heart'] . '
      <i id="' . $reponse['id_actus'] . '" class="fa-lg fa fa-heart" aria-hidden="true"></i>
      </span>
      </div>
      <img src="administration/traitement/' . $reponse['picture_url'] . ' ">
      <figcaption style="background-color: rgba(0,0,0, 0.7)">
      ' . $reponse['description'] . '
      </figcaption>
      <a href="actualites.php?idactu='.$reponse['id_actus'].'"></a>
      </figure>
      ';
    }
    $i++;
  }
  ?>

</section>
