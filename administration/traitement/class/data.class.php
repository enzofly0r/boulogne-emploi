<?php

require 'ident.php';

class data {

///////////////////////////////////////////////////////// PROPRIETÉS /////////////////////////////////////////////////////////
// DONNÉES DE CONNEXION À LA DATABASE
    private $_host = DB_HOST;
    private $_user = DB_USER;
    private $_pass = DB_PASS;
    private $_pdo;
// TABLE MEMBRES DATABASE
    private $_tableMembres;

///////////////////////////////////////////////////////// MÉTHODES /////////////////////////////////////////////////////////
// INSTANTIATION DE LA CONNEXION À LA BASE
    public function __construct() {
        $this->_pdo = new PDO($this->_host, $this->_user, $this->_pass);
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_tableMembres = "be_users";
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// CONNEXION /////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FORMAT EMAIL
    public function regexMail($mailconnect) {
        return preg_match("#^[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]{2,}\.[a-z]{2,}$#", $mailconnect);
    }

// SI LE COMPTE EXISTE
    public function verifAccount($mailconnect, $password) {
        $req = $this->_pdo->prepare("SELECT * FROM $this->_tableMembres WHERE email='" . $mailconnect . "'");
        $req->execute();
        $result = $req->fetch(PDO::FETCH_OBJ);
        return password_verify($password, $result->password);
    }

// DEFINITION DES SESSIONS
    public function connectSession($mailconnect) {
        $req = $this->_pdo->prepare("SELECT * FROM $this->_tableMembres WHERE email='" . $mailconnect . "'");
        $req->execute();
        $result = $req->fetch(PDO::FETCH_OBJ);
        $_SESSION['id_user'] = $result->id_users;
        $_SESSION['workplace'] = $result->workplace;
        $_SESSION['status'] = $result->status;
        $_SESSION['rank'] = $result->rank;
        $_SESSION['email'] = $mailconnect;
        $_SESSION['firstname'] = $result->name;
        $_SESSION['lastname'] = $result->lastname;
        $_SESSION['picture_url'] = $result->picture_url;
    }

    // ERREURS CONNEXION
    public function errorCon($error) {
        $_SESSION['errorCon'] = $error;
        header("Location: ../index.php");
    }

    // AFFICHE PHOTO CONNEXION
    public function affichePhotoConnect($check) {
        $test = 'SELECT picture_url FROM be_users WHERE email ="' . $check . '"';
        $req = $this->_pdo->prepare($test);
        $req->execute();
        return $req->fetchAll();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// INSCRIPTION ADMINISTRATEUR //////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CRYPTAGE PASSWORD
    public function cryptPassword($password) {
        return password_hash($password, PASSWORD_BCRYPT);
    }

// SI L'UTILISATEUR EXISTE
    public function userExist($name, $lastname, $email) {
        $req = $this->_pdo->prepare("SELECT name, lastname, email FROM $this->_tableMembres WHERE name='" . $name . "' AND lastname='" . $lastname . "' AND email='" . $email . "' LIMIT 1 ");
        $req->execute();
        return $req->rowCount();
    }

// INSERTION UTILISATEUR
    public function insertUser($name, $lastname, $email, $password, $rank, $workplace, $status) {
        $req = $this->_pdo->prepare("INSERT INTO $this->_tableMembres (name, lastname, email, password, rank, workplace, status) VALUES(?, ?, ?, ?, ?, ?, ?)");
        $req->execute(array($name, $lastname, $email, $password, $rank, $workplace, $status));
    }

// AFFICHAGE UTILISATEUR
    public function afficheUser() {
        $req = $this->_pdo->prepare("SELECT * FROM $this->_tableMembres");
        $req->execute();
        return $req->fetchAll();
    }

    //  SUPPRESSION RESSOURCES
    public function deleteAdmin($id) {
        $req = $this->_pdo->prepare("DELETE FROM be_users WHERE id_users ='" . $id . "' ");
        $req->execute();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// RESSOURCES /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INSERTION RESSOURCES
    public function insertRessource($titre, $description, $contenu, $catego, $picture) {
        $req = $this->_pdo->prepare("INSERT INTO be_ressources (ressources_cat_id, title_ressource, description, content_ressource, picture_ressource) VALUES (?, ?, ?, ?, ?)");
        $req->execute(array($catego, $titre, $description, $contenu, $picture));
    }

// AFFICHAGE RESSOUCES
    public function afficheRessource() {
        $req = $this->_pdo->prepare("SELECT * FROM be_ressources_cat");
        $req->execute();
        return $req->fetchAll();
    }

    // UPDATE Ressources
    public function updateRessources($idRessources, $ressourcesCat, $ressourcesTitre, $description, $ressourcesContenu, $picture) {
        $req = $this->_pdo->prepare("UPDATE be_ressources SET ressources_cat_id='" . $ressourcesCat . "', title_ressource='" . $ressourcesTitre . "', description='" . $description . "', content_ressource='" . $ressourcesContenu . "', picture_ressource='" . $picture . "' WHERE id_ressources='" . $idRessources . "' ");
        $req->execute();
    }

    // UPDATE Ressources SANS IMG
    public function updateRessourcesNoImg($idRessources, $ressourcesCat, $ressourcesTitre, $description, $ressourcesContenu) {
        $req = $this->_pdo->prepare("UPDATE be_ressources SET ressources_cat_id='" . $ressourcesCat . "', title_ressource='" . $ressourcesTitre . "', description='" . $description . "', content_ressource='" . $ressourcesContenu . "' WHERE id_ressources='" . $idRessources . "' ");
        $req->execute();
    }

    public function afficheRessourceId($id) {
        $req = $this->_pdo->prepare("SELECT * FROM be_ressources WHERE id_ressources='" . $id . "' ");
        $req->execute();
        return $result = $req->fetch(PDO::FETCH_OBJ);
    }

    //  SUPPRESSION RESSOURCES
    public function deleteRessource($id) {
        $req = $this->_pdo->prepare("DELETE FROM be_ressources WHERE id_ressources ='" . $id . "' ");
        $req->execute();
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// ACTUALITÉS /////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AFFICHAGE ACTUALITÉS
    public function afficheActu() {
        $req = $this->_pdo->prepare("SELECT * FROM be_actualites_cat");
        $req->execute();
        return $req->fetchAll();
    }

// INSERTION ACTUALITÉS
    public function insertActu($actuCat, $actuTitre, $actuDescription, $actuContenu, $important, $picture, $users_id) {
        $req = $this->_pdo->prepare("INSERT INTO be_actualites (actus_cat_id, title_actu, description, content_actu, importante, picture_url, users_id) VALUES(?, ?, ?, ?, ?, ?, ?)");
        $req->execute(array($actuCat, $actuTitre, $actuDescription, $actuContenu, $important, $picture, $users_id));
    }

//  SUPPRESSION ACTUALITES
    public function deleteActu() {
        $req = $this->_pdo->prepare('DELETE FROM be_actualites WHERE id_actus =' . $_POST['deleteactus'] . ' ');
        $req->execute();
    }

    // IMAGE EXISTE DÉJÀ
    public function imgExist($picture_url) {
        $req = $this->_pdo->prepare("SELECT picture_url FROM be_actualites WHERE picture_url='" . $picture_url . "'");
        $req->execute();
        return $req->rowCount();
    }

// UPDATE ACTUS
    public function updateActus($idactu, $actuCat, $actuTitre, $actuDescription, $actuContenu, $picture, $users_id) {
        $req = $this->_pdo->prepare("UPDATE be_actualites SET actus_cat_id='" . $actuCat . "', title_actu='" . $actuTitre . "', description='" . $actuDescription . "', content_actu='" . $actuContenu . "', picture_url='" . $picture . "', users_id='" . $users_id . "' WHERE id_actus='" . $idactu . "' ");
        $req->execute();
    }

// UPDATE ACTUS SANS IMG
    public function updateActusNoImg($idactu, $actuCat, $actuTitre, $actuDescription, $actuContenu, $users_id) {
        $req = $this->_pdo->prepare("UPDATE be_actualites SET actus_cat_id='" . $actuCat . "', title_actu='" . $actuTitre . "', description='" . $actuDescription . "', content_actu='" . $actuContenu . "', users_id='" . $users_id . "' WHERE id_actus='" . $idactu . "' ");
        $req->execute();
    }

    public function afficheActuId($id) {
        $req = $this->_pdo->prepare("SELECT * FROM be_actualites WHERE id_actus='" . $id . "' ");
        $req->execute();
        return $result = $req->fetch(PDO::FETCH_OBJ);
    }

}
